package marcos.thread;

public class Main {
    static class startThread extends Thread {
        public void run() {
            System.out.println("Second Step - Thread");
        }
    }

    static class startRunnable implements Runnable {
        public void run() {
            System.out.println("Third Step - Runnable");
        }
    }

}
