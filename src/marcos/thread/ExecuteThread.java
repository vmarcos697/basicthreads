package marcos.thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ExecuteThread {

    public static void main(String[] args) {

        System.out.println("First Step - Starting...");

        Thread first1 = new Main.startThread();
        first1.start();

        Thread second2 = new Thread(new Main.startRunnable());
        second2.start();


            try {
                Thread.sleep(2000);
                System.out.println("Fourth Step - After 2 seconds sleep");
            } catch (InterruptedException e) {
                System.out.println("Exception works " + e);
            }


        ExecutorService exe = Executors.newSingleThreadExecutor();
        exe.submit(() -> {
            String threadName = Thread.currentThread().getName();
            System.out.println("Sixth Step - Name of the pool and thread: " + threadName);
        });


            try {
                System.out.println("Fifth Step - Try to shutdown exe");
                exe.shutdown();
                exe.awaitTermination(10, TimeUnit.SECONDS);
            }
            catch (InterruptedException e) {
                System.err.println("Can't shutdown");
            }
            finally {
                if (!exe.isTerminated()) {
                    System.err.println("Try again");
                }
                exe.shutdownNow();
                System.out.println("Seventh Step - Successfully shutdown exe");
            }

    }
}
